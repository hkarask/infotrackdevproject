﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CEOToolkit.Core.Services;
using Microsoft.AspNetCore.Mvc;
using CEOToolkit.Web.Models;
using Microsoft.Extensions.Caching.Memory;

namespace CEOToolkit.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMemoryCache _cache;
        private readonly GoogleScraperService _googleScraperService;

        public HomeController(IMemoryCache cache, GoogleScraperService googleScraperService)
        {
            _cache = cache;
            _googleScraperService = googleScraperService;
        }

        public async Task<IActionResult> Index(IndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model.Rankings = await _cache.GetOrCreateAsync(
                        $"{nameof(HomeController)}_{nameof(Index)}_{model.Query}_{model.Url}", entry =>
                    {
                        entry.SlidingExpiration = TimeSpan.FromHours(4);
                        return _googleScraperService.GetRankings(model.Query, model.Url);
                    });
                }
                catch (Exception e)
                {
                    model.Error = e.Message;
                }
            }

            if (string.IsNullOrEmpty(model.Query) && string.IsNullOrEmpty(model.Url))
                ModelState.Clear();

            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            });
        }
    }
}
