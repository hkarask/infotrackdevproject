﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CEOToolkit.Web.Models
{
    public class IndexViewModel
    {
        [Required, MinLength(2, ErrorMessage = "Query min length: 2")]
        public string Query { get; set; }

        [Required, MinLength(2, ErrorMessage = "Url min length: 2")]
        public string Url { get; set; }

        public List<int> Rankings { get; set; }

        public string Error { get; set; }
    }
}