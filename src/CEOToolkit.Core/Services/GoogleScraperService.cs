﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CEOToolkit.Core.Services
{
    public class GoogleScraperService
    {
        private readonly GoogleSearch _googleSearch;

        public GoogleScraperService(HttpClient httpClient)
        {
            _googleSearch = new GoogleSearch(httpClient);
        }

        /// <summary>
        /// Ranking for n pages
        /// </summary>
        /// <param name="query">keywords</param>
        /// <param name="url">ranking url to check for</param>
        /// <param name="nrOfPages">nr of pages to check</param>
        /// <returns>Positions where page is ranked</returns>
        public async Task<List<int>> GetRankings(string query, string url, int nrOfPages = 10)
        {
            if (string.IsNullOrEmpty(query))
                throw new ArgumentException($"{query} must be set");

            if (string.IsNullOrEmpty(url))
                throw new ArgumentException($"{url} must be set");

            var tasks = Enumerable
                .Range(0, 10)
                .Select(x => GetRankingsForPage(query, url, x))
                .ToList();

            await Task.WhenAll(tasks);

            return tasks.SelectMany(x => x.Result).OrderBy(x => x).ToList();
        }

        /// <summary>
        /// Ranking positions of a given page
        /// </summary>
        /// <param name="query">keywords</param>
        /// <param name="url">ranking url to check for</param>
        /// <param name="page">page number</param>
        /// <returns>Positions where page is ranked</returns>
        public async Task<List<int>> GetRankingsForPage(string query, string url, int page)
        {
            if (string.IsNullOrEmpty(query))
                throw new ArgumentException($"{query} must be set");

            if (string.IsNullOrEmpty(url))
                throw new ArgumentException($"{url} must be set");

            string payload = await _googleSearch.FetchPageAsync(query, page);
            List<string> cites = _googleSearch.GetOrganicCites(payload);

            List<int> res = new List<int>();
            for (var i = 0; i < cites.Count; i++)
            {
                if (cites[i].IndexOf(url, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    res.Add(i 
                            + 1 // convert to human-index (starts from 1)
                            + page * 10 // adjust with page count
                    );
                }
            }

            return res;
        }
        
    }
}