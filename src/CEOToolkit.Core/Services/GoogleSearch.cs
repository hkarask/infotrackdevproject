﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CEOToolkit.Core.Services
{
    public class GoogleSearch
    {
        private readonly HttpClient _httpClient;

        public GoogleSearch(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Gets raw HTML from google search
        /// </summary>
        /// <param name="query">Search input</param>
        /// <param name="page">Page nr</param>
        /// <returns>Raw html</returns>
        public async Task<string> FetchPageAsync(string query, int page)
        {
            if (string.IsNullOrEmpty(query))
                throw new ArgumentException($"{query} must be set");

            if (page < 0)
                throw new ArgumentException($"{page} must be >= 0");

            // encoded canonical location of Sydney to get same search results accross different datacenters
            string locationSydney = "w+CAIQICIgU3lkbmV5LE5ldyBTb3V0aCBXYWxlcyxBdXN0cmFsaWE=";

            return await _httpClient.GetStringAsync($"https://www.google.com.au/search?q={query}&start={page}0&uule={locationSydney}");
        }

        /// <summary>
        /// Returns all cites withing the organic are of the search results
        /// </summary>
        /// <param name="searchPayload">raw HTML from Google search</param>
        public List<string> GetOrganicCites(string searchPayload)
        {
            if (string.IsNullOrEmpty(searchPayload))
                throw new ArgumentException($"{searchPayload} must be set");

            int organicAdStart = searchPayload.IndexOf("<div class=\"med\" id=\"res\" role=\"main\">", StringComparison.OrdinalIgnoreCase);
            if (organicAdStart <= 0)
                throw new Exception($"{nameof(organicAdStart)} index should be > 0");

            int organicAdEnd = searchPayload.IndexOf("<div id=\"bottomads\">", StringComparison.OrdinalIgnoreCase);
            if (organicAdEnd <= 0)
                throw new Exception($"{nameof(organicAdEnd)} index should be > 0");

            searchPayload = searchPayload.Substring(organicAdStart, organicAdEnd - organicAdStart);

            // Extract <cite> content (links) from the results
            var regex = new Regex(@"<cite[\sa-zA-Z0-9"" =\-_]*>(.+?)<\/cite>", RegexOptions.Multiline);

            return regex.Matches(searchPayload).Select(x => x.Groups[1].Value).ToList();
        }
    }
}