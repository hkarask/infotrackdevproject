FROM microsoft/aspnetcore-build AS build-env
ARG VERSION

WORKDIR /app

# Copy csproj and restore as distinct layers
COPY src/CEOToolkit.Web/CEOToolkit.Web.csproj ./src/CEOToolkit.Web/
COPY tests/CEOToolkit.Services.Tests ./tests/CEOToolkit.Services.Tests/
RUN dotnet restore src/CEOToolkit.Web/CEOToolkit.Web.csproj
RUN dotnet restore tests/CEOToolkit.Services.Tests/CEOToolkit.Services.Tests.csproj

# Copy everything else and build
COPY . ./
RUN dotnet test tests/CEOToolkit.Services.Tests/CEOToolkit.Services.Tests.csproj
RUN dotnet publish -c Release -o out src/CEOToolkit.Web/CEOToolkit.Web.csproj

# Build runtime image
FROM microsoft/aspnetcore

WORKDIR /app
COPY --from=build-env /app/src/CEOToolkit.Web/out ./
ENTRYPOINT ["dotnet", "CEOToolkit.Web.dll"]