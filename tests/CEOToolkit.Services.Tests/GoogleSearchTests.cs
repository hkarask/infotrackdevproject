using System.Collections.Generic;
using System.Net.Http;
using CEOToolkit.Core.Services;
using Shouldly;
using Xunit;

namespace CEOToolkit.Services.Tests
{
    public class GoogleSearchTests
    {
        private readonly GoogleSearch _googleSearch;
        private readonly string _rawHtml;

        public GoogleSearchTests()
        {
            _googleSearch = new GoogleSearch(new HttpClient(new MockHttpMessageHandler()));
            _rawHtml = _googleSearch.FetchPageAsync("online title search", 1).GetAwaiter().GetResult();
        }

        [Fact]
        public void GetOrganicCitesFinds10Cites()
        {
            List<string> cites = _googleSearch.GetOrganicCites(_rawHtml);

            cites.Count.ShouldBe(10);
        }
    }
}
