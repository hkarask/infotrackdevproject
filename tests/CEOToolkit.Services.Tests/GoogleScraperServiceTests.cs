﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using CEOToolkit.Core.Services;
using Shouldly;
using Xunit;

namespace CEOToolkit.Services.Tests
{
    public class GoogleScraperServiceTests
    {
        private readonly GoogleScraperService _googleScraperService;

        public GoogleScraperServiceTests()
        {
            // Arrange
            _googleScraperService = new GoogleScraperService(new HttpClient(new MockHttpMessageHandler()));
        }

        [Fact]
        public async Task GetInfoTrackCitesForPageReturnsCorrectIndices()
        {
            // Act
            List<int> rankings = await _googleScraperService.GetRankingsForPage("online title search", "infotrack.com.au", 3);

            // Assert
            rankings[0].ShouldBe(32);
        }

        [Fact]
        public async Task GetInfoTrackRankingsReturnsCorrectIndices()
        {
            List<int> ranking = await _googleScraperService.GetRankings("online title search", "infotrack.com.au");

            ranking.Count.ShouldBe(10);
            ranking.ToArray().ShouldBe(new []{ 2, 12, 22, 32, 42, 52, 62, 72, 82, 92 });
        }
    }
}