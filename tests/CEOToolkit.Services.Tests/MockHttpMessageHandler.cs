using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CEOToolkit.Services.Tests
{
    /// <summary>
    /// Very basic mock for that testassigment, for more complex one I'd use https://github.com/richardszalay/mockhttp
    /// </summary>
    public class MockHttpMessageHandler : HttpMessageHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string response = File.ReadAllText("GoogleSearchResponse.txt");

            return Task.Run(() => new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(response)
            }, cancellationToken);
        }
    }
}