# InfoTrack dev project

## About

This tool will ask a string of keywords and an url as an input and will check the first 100 **organic** matches from google.com.au.

Website is running on ASP.NET MVC Core 2 and is styled with Bootstrap 4. Clientside validation is done with HTML `required` attributes, because the CEO is using the latest browser. Businesslogic is encapsulated in the `CEOToolkit.Core` project, which is covered with unittests.

Queries against Google are cached in-memory for 4 hours and pages are fetched in parallel. If the web would be served from multiple instances, then distributed cache (e.g. Redis) is needed.

## Running with docker

### Building

Building is in multiple stages:

*Stage 1* compiles, tests and publishes the application by using the microsoft/aspnetcore-build image.

*Stage 2* copies the published application from Stage 1 into the final image leaving behind all of the source code and tooling needed to build.

To build the docker image:
`docker build -t infotrackdevproject .`

### Running the container

To run the built image: 
`docker run -d -p 5000:80 --name infotrackdevprojectweb infotrackdevproject`
